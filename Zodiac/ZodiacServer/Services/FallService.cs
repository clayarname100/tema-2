﻿using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using System.Globalization;

namespace ZodiacServer.Services
{
    public class FallService : FallUser.FallUserBase
    {
        private readonly ILogger<FallService> _logger;

        public FallService(ILogger<FallService> logger)
        {
            _logger = logger;
        }

        public override Task<FallSignReply> SayFallSign(FallUserRequest request, ServerCallContext context)
        {
            string zodiacSign = "";
            string[] dateFormats = { "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy" };
            DateTime requestDate = DateTime.ParseExact(request.Date, dateFormats, CultureInfo.InvariantCulture);
            string fileLine = "";
            System.IO.StreamReader file = new System.IO.StreamReader(@".\Resources\Fall.txt");
            while((fileLine = file.ReadLine()) != null )
            {
                string[] intervals = fileLine.Split(new char[] { ' ', '/' });
                DateTime date1 = new DateTime(requestDate.Year, int.Parse(intervals[0]), int.Parse(intervals[1]));
                DateTime date2 = new DateTime(requestDate.Year, int.Parse(intervals[2]), int.Parse(intervals[3]));

                if(requestDate >= date1 && requestDate <= date2)
                {
                    zodiacSign = intervals[4];
                    return Task.FromResult(new FallSignReply { Message = zodiacSign });
                }
            }
            return Task.FromResult(new FallSignReply { Message = "Whoops" });
        }
    }
}
