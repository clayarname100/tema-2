﻿using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using System.Globalization;

namespace ZodiacServer.Services
{
    public class WinterService : WinterUser.WinterUserBase
    {
        private readonly ILogger<WinterService> _logger;

        public WinterService(ILogger<WinterService> logger)
        {
            _logger = logger;
        }

        public override Task<WinterSignReply> SayWinterSign(WinterUserRequest request, ServerCallContext context)
        {
            string zodiacSign = "";
            string[] dateFormats = { "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy" };
            DateTime requestDate = DateTime.ParseExact(request.Date, dateFormats, CultureInfo.InvariantCulture);
            switch (requestDate.Month)
            {
                case 12:
                    if (requestDate.Day < 22)
                        zodiacSign = "Sagetator";
                    else
                        zodiacSign = "Capricorn";
                    break;
                case 1:
                    if (requestDate.Day < 20)
                        zodiacSign = "Capricorn";
                    else
                        zodiacSign = "Varsator";
                    break;
                case 2:
                    if (requestDate.Day < 19)
                        zodiacSign = "Varsator";
                    else
                        zodiacSign = "Pesti";
                    break;
                default:
                    zodiacSign = "Whoops";
                    break;
            }
            return Task.FromResult(new WinterSignReply { Message = zodiacSign });
        }
    }
}