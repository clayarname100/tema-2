﻿using Grpc.Net.Client;
using System;
using System.Globalization;
using System.Threading.Tasks;
using ZodiacServer;

namespace Zodiac_Client
{
    class Program
    {
        public static bool isValid(string dateString)
        {
            DateTime date;
            string[] dateFormats = { "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy" };
            return DateTime.TryParseExact(dateString, dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        }
        static async Task Main(string[] args)
        {
            string address = "https://localhost:5001";
            var channel = GrpcChannel.ForAddress(address);
            bool isRunning = true;
            while(isRunning)
            {
                string date = "";
                Console.Write("Introduceti o data noua sau '0' pentru a opri programul: ");
                date = Console.ReadLine();
                if(date == "0")
                {
                    isRunning = false;
                    break;
                }
                if (!isValid(date))
                {
                    Console.WriteLine("Data introdusa nu este valida!");
                    continue;
                }
                int month;
                month = int.Parse(date.Substring(0, date.IndexOf('/')));
                switch (month)
                {
                    case 12:
                    case 1:
                    case 2:
                        var winterInput = new WinterUserRequest { Date = date };
                        var winterClient = new WinterUser.WinterUserClient(channel);
                        var winterReply = await winterClient.SayWinterSignAsync(winterInput);
                        Console.WriteLine(winterReply.Message);
                        break;
                    case 3:
                    case 4:
                    case 5:
                        var springInput = new SpringUserRequest { Date = date };
                        var springClient = new SpringUser.SpringUserClient(channel);
                        var springReply = await springClient.SaySpringSignAsync(springInput);
                        Console.WriteLine(springReply.Message);
                        break;
                    case 6:
                    case 7:
                    case 8:
                        var summerInput = new SummerUserRequest { Date = date };
                        var summerClient = new SummerUser.SummerUserClient(channel);
                        var summerReply = await summerClient.SaySummerSignAsync(summerInput);
                        Console.WriteLine(summerReply.Message);
                        break;
                    case 9:
                    case 10:
                    case 11:
                        var fallInput = new FallUserRequest { Date = date };
                        var fallClient = new FallUser.FallUserClient(channel);
                        var fallReply = await fallClient.SayFallSignAsync(fallInput);
                        Console.WriteLine(fallReply.Message);
                        break;
                    default:
                        Console.WriteLine("Whoops");
                        break;
                }
            }
        }
    }
}
